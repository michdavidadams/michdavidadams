---
title: "Panucci's Pizza"
description: "A pizza ordering system."
header_image: /img/panuccispizza/pizza.png
tags: ["apps", "portfolio", "school"]
date: "2021-10-02"
---
A pizza ordering system.

![Pizza menu](/img/panuccispizza/pizza.png)
![Checkout screen](/img/panuccispizza/checkout.png)

*Built in Swift and SwiftUI using Xcode. 2021*
