---
title: "Woof"
description: "Track workouts with your dog."
header_image: /img/WoofHeader.png
date: "2022-03-01"
tags: ["apps", "portfolio"]
---
![Start view](/img/woof/StartView.png)
![Walk view](/img/woof/WalkView.png)
![Play view](/img/woof/PlayView.png)

Woof is a workout tracker that allows you to track your walks and playtime with your dog.

View your dog's progress towards their daily exercise goal and view their streak.

During a walk, workouts will automatically be paused when your watch detects you've stopped moving for a bit.

Woof works with HealthKit to display your workout metrics and save your workouts.


<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/702200557?h=756f8d206d&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Woof Workouts demonstration"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Support
If you need help, email <me@michdavidadams.com>

### FAQ
- How does the autopause feature work?
    - It uses the watchOS pedometer to detect when you've stopped moving for a bit, or when you've started moving.
- It crashes.
    - Check to make sure you've enabled the following:
        - HealthKit read/write access
        - Apple Watch Motion & Activity access
        - Location When In Use access

## Privacy Policy
No data is collected, I mind my own business.
