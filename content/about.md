# About
![Selfie](/selfie.jpg)
I'm a computer science student. Associates from Georgia Highlands College, working on my bachelors from Kennesaw State University.

---

[Email](mailto:me@michdavidadams.com)

[Twitter](https://twitter.com/michdavidadams)

[Steam](https://steamcommunity.com/id/michdavidadams/)

[Trakt](https://trakt.tv/users/michdavidadams)

[Last.fm](https://www.last.fm/user/michdavidadams) / [ListenBrainz](https://listenbrainz.org/user/michdavidadams/)

